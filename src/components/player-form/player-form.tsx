import { Component, State, h } from '@stencil/core';

@Component({
  tag: 'player-form',
  styleUrl: 'player-form.css'
})
export class PlayerForm {
  @State() list = []
  @State() value: string;

  handleSubmit(e) {
    e.preventDefault()
    console.log(this.value);
    this.list = [...this.list, this.value];
  }

  handleChange(event) {
    this.value = event.target.value;
  }

  renderNames() {
    return this.list.map((name) => {
      return (
        <ion-list>
          <ion-list-header>
            <ion-label>Name</ion-label>
          </ion-list-header>
          <ion-item>{name}</ion-item>
        </ion-list>
    }))
  }

  render() {
    return [
      <div>
      <form onSubmit={(e) => this.handleSubmit(e)}>
        <label>
          Name:
          <input type="text" value={this.value} onInput={(event) => this.handleChange(event)} />
        </label>
        <input type="submit" value="Submit" />
      </form>
      </div>,
      <div>
        {this.renderNames()}
      </div>
    ];
  }
}
